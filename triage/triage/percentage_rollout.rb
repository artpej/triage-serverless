# frozen_string_literal: true

require 'digest'

module Triage
  module PercentageRollout
    # Applies a incremental roll out to triage requests
    # based on a given threshold value.
    #
    # It requires a string event identifier that is used
    # to seed a random integer and compare it against the threshold.
    #
    # Example:
    #
    #   MyReaction < Triager
    #     include PercentageRollout
    #
    #     percentage_rollout 20, on: ->(event) { event.id.to_s }
    #   end

    def self.included(klass)
      klass.extend(ClassMethods)
      klass.prepend(Hooks)
    end

    module ClassMethods
      attr_reader :threshold, :identifier_lambda

      def percentage_rollout(threshold, on:)
        @threshold = threshold
        @identifier_lambda = on
      end
    end

    module Hooks
      def applicable?
        super && rolled_out?
      end

      def rolled_out?
        return true unless self.class.identifier_lambda && self.class.threshold

        score <= self.class.threshold
      end

      private

      def score
        @score ||= Random.new(seed).rand(100)
      end

      def seed
        @seed ||= Digest::MD5.hexdigest(identifier).to_i(16)
      end

      def identifier
        @identifier ||= self.class.identifier_lambda.call(event).to_s
      end
    end
  end
end
