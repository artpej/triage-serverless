# frozen_string_literal: true

require 'gitlab'

module Triage
  class Triager
    attr_reader :event

    def self.triage(event)
      new(event).triage
    end

    def initialize(event)
      @event = event
    end

    def triage
      return unless applicable?

      before_process
      process
      after_process
    end

    def process
      raise NotImplementedError
    end

    private

    def add_comment(body)
      path = "#{event.noteable_path}/notes"

      post_request(path, body)
    end

    def add_discussion(body)
      path = "#{event.noteable_path}/discussions"

      post_request(path, body)
    end

    def post_request(path, body)
      if Triage.dry_run?
        puts "The following comment would have been posted to #{PRODUCTION_API_ENDPOINT}#{path}:"
        puts "```\n#{body}\n```"
      else
        Triage.api_client.post(path, body: { body: body })
      end
    end

    def applicable?
      true
    end

    def before_process
      nil
    end

    def after_process
      nil
    end
  end
end
