# frozen_string_literal: true

require 'gitlab'
require 'time'

require_relative 'error'

module Triage
  class Event
    ObjectKindNotProvidedError = Class.new(Triage::ClientError)

    # https://gitlab.com/gitlab-renovate-bot
    GITLAB_RENOVATE_BOT_ID = 8420142

    # https://gitlab.com/project_24673064_bot
    CONTAINER_SCANNING_BOT_ID = 8935721

    # Automation users that are not part of the GitLab groups for security reasons
    AUTOMATION_IDS = [
      GITLAB_RENOVATE_BOT_ID,
      CONTAINER_SCANNING_BOT_ID
    ].freeze

    ISSUE = 'issue'
    MERGE_REQUEST = 'merge_request'
    NOTE = 'note'

    GITLAB_PROJECT_ID = 278964

    attr_reader :event

    def initialize(event)
      @event = event
    end

    def self.build(event)
      event['object_kind'].yield_self do |kind|
        if kind.nil?
          raise ObjectKindNotProvidedError, '`object_kind` not provided!'
        end

        case kind
        when ISSUE
          IssueEvent.new(event)
        when MERGE_REQUEST
          MergeRequestEvent.new(event)
        when NOTE
          NoteEvent.new(event)
        else
          raise "Unknown object: #{kind.inspect}"
        end
      end
    end

    def issue?
      object_kind == ISSUE
    end

    def merge_request?
      object_kind == MERGE_REQUEST
    end

    def note?
      object_kind == NOTE
    end

    def user
      event.fetch('user', {})
    end

    def user_username
      user['username']
    end

    def author_id
      event.dig('object_attributes', 'author_id')
    end

    def created_at
      created_at_timestamp = event.dig('object_attributes', 'created_at')
      Time.parse(created_at_timestamp)
    end

    def url
      event.dig('object_attributes', 'url')
    end

    def new_comment
      event.dig('object_attributes', 'description').to_s
    end

    def new_entity?
      event.dig('object_attributes', 'action') == 'open'
    end

    def added_label_names
      current_label_names - previous_label_names
    end

    def label_names
      labels.map { |label_data| label_data['title'] }
    end

    def noteable_path
      "/projects/#{project_id}/#{noteable_kind}/#{noteable_iid}"
    end

    def from_gitlab_org?
      event_from_group?(Triage::GITLAB_ORG_GROUP)
    end

    def from_gitlab_com?
      event_from_group?(Triage::GITLAB_COM_GROUP)
    end

    def from_gitlab_org_gitlab?
      with_project_id?(GITLAB_PROJECT_ID)
    end

    def automation_author?
      AUTOMATION_IDS.include?(author_id)
    end

    def wider_community_author?
      !automation_author? &&
        (!Triage.gitlab_org_group_member_ids.include?(author_id) || Triage.gitlab_core_team_community_members_group_member_ids.include?(author_id))
    end

    def wider_gitlab_com_community_author?
      !automation_author? &&
        (!Triage.gitlab_com_group_member_ids.include?(author_id) || Triage.gitlab_core_team_community_members_group_member_ids.include?(author_id))
    end

    def jihu_contributor?
      Triage.jihu_team_member_ids.include?(author_id)
    end

    def with_project_id?(expected_project_id)
      project_id == expected_project_id
    end

    def project_id
      event.dig('project', 'id')
    end

    def project_web_url
      event.dig('project', 'web_url')
    end

    def project_path_with_namespace
      event.dig('project', 'path_with_namespace')
    end

    def title
      event.dig('object_attributes', 'title')
    end

    private

    def object_kind
      event.fetch('object_kind', 'unknown')
    end

    def noteable_kind
      raise NotImplementedError
    end

    def noteable_iid
      event.dig('object_attributes', 'iid')
    end

    def changes
      event.fetch('changes', {})
    end

    def labels
      event.fetch('labels', [])
    end

    def previous_labels
      changes.dig('labels', 'previous') || []
    end

    def current_labels
      if new_entity?
        labels
      else
        changes.dig('labels', 'current') || []
      end
    end

    def previous_label_names
      previous_labels.map { |l| l['title'] }
    end

    def current_label_names
      current_labels.map { |l| l['title'] }
    end

    def event_from_group?(group)
      %r{\A#{group}/}.match?(event.dig('project', 'path_with_namespace'))
    end
  end

  class IssueEvent < Event
    def iid
      event.dig('object_attributes', 'iid')
    end

    private

    def noteable_kind
      'issues'
    end
  end

  class MergeRequestEvent < Event
    def iid
      event.dig('object_attributes', 'iid')
    end

    def wip?
      event.dig('object_attributes', 'work_in_progress')
    end

    def merge_event?
      event.dig('object_attributes', 'action') == 'merge'
    end

    def approval_event?
      event.dig('object_attributes', 'action') == 'approval'
    end

    def approved_event?
      event.dig('object_attributes', 'action') == 'approved'
    end

    private

    def noteable_kind
      'merge_requests'
    end
  end

  class NoteEvent < Event
    def new_comment
      event.dig('object_attributes', 'note').to_s
    end

    def noteable_author_id
      event.dig(noteable_type, 'author_id')
    end

    def note_on_issue?
      noteable_type == ISSUE
    end

    def note_on_merge_request?
      noteable_type == MERGE_REQUEST
    end

    private

    def noteable_type
      to_snake_case(event.dig('object_attributes', 'noteable_type'))
    end

    def noteable_kind
      "#{noteable_type}s"
    end

    def noteable_iid
      event.dig(noteable_type, 'iid')
    end

    def to_snake_case(string)
      string.gsub(/(?<=[a-z])[A-Z]/, '_\0').downcase
    end
  end
end
