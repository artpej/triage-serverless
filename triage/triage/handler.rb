# frozen_string_literal: true

require_relative '../processor/availability_priority'
require_relative '../processor/backstage_label'
require_relative '../processor/customer_label'
require_relative '../processor/customer_contribution_merged_notifier'
require_relative '../processor/customer_contribution_created_notifier'
require_relative '../processor/deprecated_label'
require_relative '../processor/hackathon_label'
require_relative '../processor/doc_community_contribution'
require_relative '../processor/merge_request_help'
require_relative '../processor/new_pipeline_on_approval'
require_relative '../processor/reactive_labeler'
require_relative '../processor/thank_community_contribution'
require_relative '../processor/jihu_contribution'
require_relative '../processor/type_label'
require_relative '../processor/merge_request_ci_title_label'

module Triage
  class Handler
    DEFAULT_TRIAGERS = [
      AvailabilityPriority,
      CustomerLabel,
      TypeLabel,
      BackstageLabel,
      DeprecatedLabel,
      ThankCommunityContribution,
      JiHuContribution,
      DocCommunityContribution,
      MergeRequestHelp,
      ReactiveLabeler,
      CustomerContributionMergedNotifier,
      CustomerContributionCreatedNotifier,
      NewPipelineOnApproval,
      HackathonLabel,
      MergeRequestCiTitleLabel
    ].freeze

    Result = Struct.new(:messages, :errors)

    def initialize(event, triagers: DEFAULT_TRIAGERS)
      @event = event
      @triagers = triagers
    end

    def process
      messages = []
      errors = []

      triagers.each do |triager|
        messages << triager.triage(event)
      rescue => e
        errors << e
      end

      Result.new(messages.compact, errors.compact)
    end

    private

    attr_reader :event, :triagers
  end
end
