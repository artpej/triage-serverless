# frozen_string_literal: true

require_relative '../triage/triager'
require_relative '../triage/rate_limit'

require 'digest'
require 'slack-messenger'

module Triage
  class MergeRequestHelp < Triager
    include RateLimit

    GITLAB_BOT = '@gitlab-bot'
    ACTION_REGEXP = /^#{Regexp.escape(GITLAB_BOT)}[[:space:]]+help/

    SLACK_CHANNEL = '#mr-coaching'
    SLACK_ICON = ':robot_face:'
    SLACK_MESSAGE_TEMPLATE = <<~MESSAGE
      Hi MR coaches, a contributor has requested help in %<comment_url>s.
    MESSAGE

    def initialize(event, messenger: slack_messenger)
      super(event)
      @messenger = messenger
    end

    def applicable?
      event.from_gitlab_org? &&
        event.note? &&
        event.note_on_merge_request? &&
        event_by_noteable_author? &&
        valid_action_prefix?
    end

    def process
      send_help_request
      send_reply_to_author
    end

    private

    attr_reader :messenger

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("help-commands-sent-#{event.user['id']}-#{event.noteable_path}")
    end

    def rate_limit_count
      1
    end

    def rate_limit_period
      86400 # 1 day
    end

    def actions
      @actions ||= event.new_comment.scan(ACTION_REGEXP)
    end

    def valid_action_prefix?
      actions.any?
    end

    def event_by_noteable_author?
      event.user['id'] == event.noteable_author_id
    end

    def send_help_request
      message = format(SLACK_MESSAGE_TEMPLATE, comment_url: event.url)
      messenger.ping(message)
    end

    def slack_messenger
      options = {
        channel: SLACK_CHANNEL,
        username: GITLAB_BOT,
        icon_emoji: SLACK_ICON
      }

      Slack::Messenger.new(ENV['SLACK_WEBHOOK_URL'], options)
    end

    def send_reply_to_author
      add_comment <<~MARKDOWN.chomp
        :wave: @#{event.user['username']},

        Thanks for reaching out for help. I've notified [the merge request coaches](https://about.gitlab.com/company/team/?department=merge-request-coach)

        They will get back to you as soon as they can.

        If you have not received any response, you may ask for help again after 1 day.
      MARKDOWN
    end
  end
end
