# frozen_string_literal: true

require_relative './customer_contribution_notifier'

module Triage
  class CustomerContributionCreatedNotifier < CustomerContributionNotifier
    def customer_contribution_message_template
      msg = <<~MESSAGE
      > New Customer MR Created -
      MESSAGE

      msg + super
    end

    def applicable?
      (event.merge_request? && event.new_entity?) && super
    end

    def slack_channel
      '#contribution-efficiency'
    end
  end
end
