# frozen_string_literal: true

require_relative './customer_contribution_notifier'

module Triage
  class CustomerContributionMergedNotifier < CustomerContributionNotifier
    def customer_contribution_message_template
      msg = <<~MESSAGE
      > Customer MR Merged -
      MESSAGE

      msg + super
    end

    def applicable?
      (event.merge_request? && event.merge_event?) && super
    end
  end
end
