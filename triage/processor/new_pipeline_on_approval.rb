# frozen_string_literal: true

require_relative '../triage/triager'
require_relative '../triage/percentage_rollout'

module Triage
  class NewPipelineOnApproval < Triager
    include PercentageRollout

    percentage_rollout 100, on: ->(event) { event.iid.to_s }

    GITLAB_PROJECT_PATH = 'gitlab-org/gitlab'
    UPDATE_GITALY_BRANCH = 'release-tools/update-gitaly'
    STABLE_BRANCH_SUFFIX = 'stable-ee'
    NOTES_PER_PAGE = 100
    HIDDEN_COMMENT = '<!-- triage-serverless NewPipelineOnApproval -->'

    def applicable?
      event.from_gitlab_org? &&
        event.merge_request? &&
        from_gitlab_project?(event) &&
        merge_request_approved?(event) &&
        need_new_pipeline?(event) &&
        no_previous_comment?
    end

    def process
      add_discussion <<~MARKDOWN.chomp
        #{HIDDEN_COMMENT}
        :wave: @#{event.user_username}, thanks for approving this merge request.
        
        Please consider starting a new pipeline if:
        - This is the first time the merge request is approved, or
        - The merge request is ready to be merged, and there has not been a merge request pipeline in the last 2 hours.
        
        For more info, refer to the [guideline](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request).
      MARKDOWN
    end

    private

    def from_gitlab_project?(event)
      event.project_path_with_namespace == GITLAB_PROJECT_PATH
    end

    # Merge request event can contain either `approved` or `approval` action
    # depending on the approval rules set for the merge request.
    # In this case, the reaction applies to either action.
    def merge_request_approved?(event)
      event.approval_event? || event.approved_event?
    end

    def need_new_pipeline?(event)
      !(gitaly_update_branch?(event) || stable_branch?(event))
    end

    def gitaly_update_branch?(event)
      event.event.dig('object_attributes', 'source_branch') == UPDATE_GITALY_BRANCH
    end

    def stable_branch?(event)
      event.event.dig('object_attributes', 'target_branch').end_with?(STABLE_BRANCH_SUFFIX)
    end

    def no_previous_comment?
      merge_request_notes.none? do |note|
        note.body.start_with?(HIDDEN_COMMENT)
      end
    end

    def merge_request_notes
      Triage.api_client.merge_request_notes(project_id, merge_request_iid, per_page: NOTES_PER_PAGE).auto_paginate
    end

    def merge_request_iid
      event.iid
    end

    def project_id
      event.project_id
    end
  end
end
