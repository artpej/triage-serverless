# frozen_string_literal: true

require_relative '../triage/triager'

module Triage
  class JiHuContribution < Triager
    LABEL_NAME = 'JiHu contribution'

    def applicable?
      event.merge_request? && event.new_entity? && event.jihu_contributor?
    end

    def process
      add_comment(%Q{/label ~"#{LABEL_NAME}"\ncc @gitlab-com/gl-security/appsec this is a ~"#{LABEL_NAME}", please follow the [JiHu contribution review process](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/jihu-contribution-review-process.html#review-workflow)})
    end
  end
end
