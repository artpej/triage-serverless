# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/handler'

RSpec.describe Triage::Handler do
  subject { described_class.new(event, triagers: [triager1, triager2]) }

  let(:event) { double('Triage::Event') }
  let(:triager1) { class_double('Triage::Triager', triage: true) }
  let(:triager2) { class_double('Triage::Triager', triage: nil) }

  describe 'DEFAULT_TRIAGERS' do
    it 'includes all triager implementations' do
      expected = [
        Triage::AvailabilityPriority,
        Triage::CustomerLabel,
        Triage::TypeLabel,
        Triage::BackstageLabel,
        Triage::DeprecatedLabel,
        Triage::ThankCommunityContribution,
        Triage::JiHuContribution,
        Triage::DocCommunityContribution,
        Triage::MergeRequestHelp,
        Triage::ReactiveLabeler,
        Triage::CustomerContributionMergedNotifier,
        Triage::CustomerContributionCreatedNotifier,
        Triage::NewPipelineOnApproval,
        Triage::HackathonLabel,
        Triage::MergeRequestCiTitleLabel
      ]

      expect(described_class::DEFAULT_TRIAGERS).to eq(expected)
    end
  end

  describe '#process' do
    it 'executes all triagers' do
      subject.process

      expect(triager1).to have_received(:triage).once
      expect(triager2).to have_received(:triage).once
    end

    it 'removes nil result from triager' do
      result = subject.process

      expect(result.messages.all?).to be_truthy
    end

    context 'when a triager raises an error' do
      let(:error) { RuntimeError.new }

      before do
        allow(triager1).to receive(:triage).and_raise(error)
      end

      it 'captures error' do
        result = subject.process

        expect(result.errors).to contain_exactly(error)
      end

      it 'executes subsequent triager' do
        subject.process

        expect(triager2).to have_received(:triage).once
      end
    end
  end
end
