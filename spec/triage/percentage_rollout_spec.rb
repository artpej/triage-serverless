# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/triager'
require_relative '../../triage/triage/percentage_rollout'

RSpec.describe Triage::PercentageRollout do
  let(:sample_size) { 1000 }
  let(:events) { (1..sample_size).map { |i| double('Triage::Event', iid: i) } }

  let!(:counter_class) do
    Class.new do
      attr_reader :count

      def initialize
        @count = 0
      end

      def add
        @count += 1
      end
    end
  end

  context 'with processors having percentage roll out' do
    let(:processor_class_30_percent) do
      Class.new(Triage::Triager) do
        include Triage::PercentageRollout

        percentage_rollout 30, on: -> (event) { event.iid.to_s }

        def initialize(event, counter)
          super(event)
          @counter = counter
        end

        def process
          @counter.add
        end
      end
    end

    let(:processor_class_50_percent) do
      Class.new(Triage::Triager) do
        include Triage::PercentageRollout

        percentage_rollout 50, on: -> (event) { event.iid.to_s }

        def initialize(event, counter)
          super(event)
          @counter = counter
        end

        def process
          @counter.add
        end
      end
    end

    let(:counter_30) { counter_class.new }
    let(:counter_50) { counter_class.new }

    it 'processes the given percentages of events' do
      events.each do |event|
        processor_30 = processor_class_30_percent.new(event, counter_30)
        processor_50 = processor_class_50_percent.new(event, counter_50)

        processor_30.triage
        processor_50.triage
      end

      expect(counter_30.count).to be_within(10).of(300)
      expect(counter_50.count).to be_within(10).of(500)
    end
  end

  context 'with processor that does not specify percentage roll out' do
    let(:processor_class) do
      Class.new(Triage::Triager) do
        include Triage::PercentageRollout
      end
    end

    let(:event) { double('Triage::Event') }
    let(:processor) { processor_class.new(event) }

    it 'calls the hooks' do
      expect(processor).to receive(:before_process)
      expect(processor).to receive(:process)
      expect(processor).to receive(:after_process)

      processor.triage
    end
  end

  context 'with processor which assumes rolling out on a specific event but it is not the event' do
    let(:processor_class) do
      Class.new(Triage::Triager) do
        include Triage::PercentageRollout

        percentage_rollout 25, on: ->(event) { event.iid.to_s }

        def applicable?
          event.merge_request?
        end
      end
    end

    let(:event) { double('Triage::Event', merge_request?: false) }
    let(:processor) { processor_class.new(event) }

    it 'does not call the hooks' do
      expect(processor).not_to receive(:before_process)
      expect(processor).not_to receive(:process)
      expect(processor).not_to receive(:after_process)

      processor.triage
    end
  end
end
