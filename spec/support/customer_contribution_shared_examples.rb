RSpec.shared_examples 'customer contribution processor slack options' do |slack_channel|
  context 'slack options' do
    let(:expected_options) do
      {
        channel: slack_channel,
        username: described_class::GITLAB_BOT,
        icon_emoji: described_class::SLACK_ICON
      }
    end

    it 'has correct slack options' do
      expect(subject.slack_options).to eq(expected_options)
    end

    context 'with default messenger' do
      it 'instantiates slack messenger with the correct options' do
        expect(Slack::Messenger).to receive(:new).with(
          ENV['SLACK_WEBHOOK_URL'],
          expected_options
        )

        described_class.new(event)
      end
    end
  end
end

RSpec.shared_examples 'no message posting' do
  it 'does not call #notify_customer_contribution_channel' do
    expect(subject).not_to receive(:notify_customer_contribution_channel)

    subject.process
  end
end

RSpec.shared_examples 'customer contribution processor #process' do
  let(:contribution_type) { type_label }

  context 'when event project is not under gitlab-org' do
    before do
      allow(event).to receive(:from_gitlab_org?).and_return(false)
    end

    it_behaves_like 'no message posting'
  end

  context 'when event is not for a merge request' do
    before do
      allow(event).to receive(:merge_request?).and_return(false)
    end

    it_behaves_like 'no message posting'
  end

  context 'when event is not from a wider community member' do
    before do
      allow(event).to receive(:wider_community_author?).and_return(false)
    end

    it_behaves_like 'no message posting'
  end

  context 'when event is not associated with an org' do
    before do
      allow(Triage::OrgByUsernameLocator).to receive(:locate_org).and_return(nil)
    end

    it_behaves_like 'no message posting'
  end

  context 'when event is for a MR authored by a customer author with org under the gitlab-org group' do
    it_behaves_like 'message posting'
  end

  context 'type labels for MR' do
    context 'feature and bug labels preferred when multiple' do
      %w[feature bug].each do |label|
        let(:contribution_type) { label }
        let(:type_label) { ['documentation', contribution_type] }

        it_behaves_like 'message posting'
      end
    end

    context 'documentation still mentioned if only type label' do
      let(:type_label) { 'documentation' }

      it_behaves_like 'message posting'
    end
  end
end
