# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/jihu_contribution'
require_relative '../../triage/triage/event'

RSpec.describe Triage::JiHuContribution do
  subject { described_class.new(event) }

  let(:event) do
    instance_double('Triage::Event',
      jihu_contributor?: false,
      merge_request?: true,
      new_entity?: true,
      noteable_path: '/moo')
  end

  describe '#applicable?' do
    context 'when event is for a new merge request opened by a JiHu contributor' do
      before do
        allow(event).to receive(:jihu_contributor?).and_return(true)
      end

      include_examples 'event is applicable'
    end

    context 'when event is not for a merge request' do
      before do
        allow(event).to receive(:merge_request?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not for a new entity' do
      before do
        allow(event).to receive(:new_entity?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not from a JiHu contributor' do
      before do
        allow(event).to receive(:jihu_contributor?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    it 'posts a message to apply the label' do
      expected_message =<<~MARKDOWN.chomp
        /label ~"JiHu contribution"
        cc @gitlab-com/gl-security/appsec this is a ~"JiHu contribution", please follow the [JiHu contribution review process](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/jihu-contribution-review-process.html#review-workflow)
      MARKDOWN

      expect_comment_request(event: event, body: expected_message) do
        subject.process
      end
    end
  end
end
