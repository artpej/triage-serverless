# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/hackathon_label'
require_relative '../../triage/triage/event'

RSpec.describe Triage::HackathonLabel do
  subject { described_class.new(event) }

  let(:added_label_names) { ['Community contribution'] }
  let(:event) do
    instance_double('Triage::MergeRequestEvent',
                    from_gitlab_org?: true,
                    merge_request?: true,
                    created_at: Time.parse('2021-09-07T12:00:00Z'),
                    added_label_names: added_label_names,
                    user: { 'username' => 'root' },
                    noteable_path: '/foo')
  end

  describe '#applicable?' do
    context 'when Community contribution label added' do
      let(:added_label_names) { ['Community contribution'] }

      context 'in gitlab-org' do
        before do
          allow(event).to receive(:from_gitlab_org?).and_return(true)
          allow(event).to receive(:from_gitlab_com?).and_return(false)
        end

        include_examples 'event is applicable'
      end

      context 'in gitlab-com' do
        before do
          allow(event).to receive(:from_gitlab_org?).and_return(false)
          allow(event).to receive(:from_gitlab_com?).and_return(true)
        end

        include_examples 'event is applicable'
      end

      context 'when event is not merge request event' do
        before do
          allow(event).to receive(:merge_request?).and_return(false)
        end

        include_examples 'event is not applicable'
      end

      context 'when merge request is created before hackathon' do
        before do
          allow(event).to receive(:created_at).and_return(Time.parse('2021-09-07T11:59:59Z'))
        end

        include_examples 'event is not applicable'
      end

      context 'when merge request is created after hackathon' do
        before do
          allow(event).to receive(:created_at).and_return(Time.parse('2021-09-09T12:00:01Z'))
        end

        include_examples 'event is not applicable'
      end
    end

    context 'when no ~"Community contribution" label added' do
      let(:added_label_names) { ['foo'] }

      include_examples 'event is not applicable'
    end

    context 'when event is neither from gitlab-org or gitlab-com' do
      let(:added_label_names) { ['Community contribution'] }

      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
        allow(event).to receive(:from_gitlab_com?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    it 'posts a message to label Hackathon' do
      body = <<~MARKDOWN.chomp
        This MR will be considered [part of](#{described_class::HACKATHON_TRACKING_ISSUE}) the quarterly GitLab Hackathon for a chance to win a [prize](#{described_class::HACKATHON_WEBPAGE}).

        Can you make sure this MR is mentioning/linking the relevant issue that it's attempting to close?

        Thank you for your contribution!

        /label ~Hackathon
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
