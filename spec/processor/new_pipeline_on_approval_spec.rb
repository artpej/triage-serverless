# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/new_pipeline_on_approval'
require_relative '../../triage/triage/event'

RSpec.describe Triage::NewPipelineOnApproval do
  subject { described_class.new(event) }

  let(:approver_username) { 'approver' }
  let(:target_branch) { 'master' }
  let(:source_branch) { 'ms-viewport' }
  let(:project_id) { 123 }
  let(:merge_request_iid) { 300 }

  let(:event) do
    instance_double('Triage::MergeRequestEvent',
                    from_gitlab_org?: true,
                    iid: merge_request_iid,
                    project_id: project_id,
                    merge_request?: true,
                    approval_event?: true,
                    approved_event?: true,
                    user_username: approver_username,
                    project_path_with_namespace: 'gitlab-org/gitlab',
                    noteable_path: '/foo',
                    event: {
                      'object_attributes' => {
                        'target_branch' => target_branch,
                        'source_branch' => source_branch,
                      }
                    })
  end

  let(:merge_request_notes) do
    [
      { "body": "review comment 1" },
      { "body": "review comment 2" }
    ]
  end

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/notes?per_page=100",
      response_body: merge_request_notes)
    allow(subject).to receive(:rolled_out?).and_return(true)
  end

  describe '#applicable?' do
    context 'when event is from gitlab-org/gitlab, with a merge request approval action' do
      before do
        allow(event).to receive(:approval_event?).and_return(true)
        allow(event).to receive(:approved_event?).and_return(false)
      end

      include_examples 'event is applicable'
    end

    context 'when event is from gitlab-org/gitlab, with a merge request approved action' do
      before do
        allow(event).to receive(:approval_event?).and_return(false)
        allow(event).to receive(:approved_event?).and_return(true)
      end

      include_examples 'event is applicable'
    end

    context 'when event is not from gitlab-org/gitlab' do
      before do
        allow(event).to receive(:project_path_with_namespace).and_return('gitlab-org/gitaly')
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not from gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not on merge request' do
      before do
        allow(event).to receive(:merge_request?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request source branch is `release-tools/update-gitaly`' do
      let(:source_branch) { 'release-tools/update-gitaly' }

      include_examples 'event is not applicable'
    end

    context 'when merge request target branch is a stable branch' do
      let(:target_branch) { '14-0-stable-ee' }

      include_examples 'event is not applicable'
    end

    context 'when merge request has a note containing hidden comment' do
      let(:merge_request_notes) do
        [
          {
            "body": "#{described_class::HIDDEN_COMMENT}",
          }
        ]
      end

      before do
        stub_api_request(
          path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/notes",
          response_body: merge_request_notes)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    it 'posts a discussion to nudge the approval to start a new pipeline' do
      body = <<~MARKDOWN.chomp
        #{described_class::HIDDEN_COMMENT}
        :wave: @#{approver_username}, thanks for approving this merge request.
        
        Please consider starting a new pipeline if:
        - This is the first time the merge request is approved, or
        - The merge request is ready to be merged, and there has not been a merge request pipeline in the last 2 hours.

        For more info, refer to the [guideline](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request).
      MARKDOWN

      expect_discussion_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
