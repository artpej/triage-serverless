# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/backstage_label'
require_relative '../../triage/triage/event'

RSpec.describe Triage::BackstageLabel do
  subject { described_class.new(event) }

  let(:event) do
    instance_double('Triage::Event',
      from_gitlab_org?: true,
      added_label_names: added_label_names,
      user: { 'username' => 'root' },
      noteable_path: '/foo')
  end

  describe '#applicable?' do
    context 'when there is no backstage label' do
      let(:added_label_names) { ['foo'] }

      include_examples 'event is not applicable'
    end

    described_class::BACKSTAGE_LABELS.each do |label|
      context 'when there is the backstage label' do
        let(:added_label_names) { [label] }

        include_examples 'event is applicable'

        context 'when event project is not under gitlab-org' do
          before do
            allow(event).to receive(:from_gitlab_org?).and_return(false)
          end

          include_examples 'event is not applicable'
        end
      end
    end
  end

  describe '#process' do
    let(:label) { described_class::BACKSTAGE_LABELS.first }
    let(:added_label_names) { [label] }

    it 'posts a deprecation message' do
      body = <<~MARKDOWN.chomp
        Hey @root, ~"#{label}" is being deprecated in favor of ~"feature::addition", ~"feature::maintenance", ~"tooling::pipelines", and ~"tooling::workflow" to improve the identification of these type of changes.
        Please see https://about.gitlab.com/handbook/engineering/metrics/#data-classification for further guidance.
        /unlabel ~"#{label}"
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
