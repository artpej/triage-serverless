# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/customer_contribution_merged_notifier'
require_relative '../../triage/triage/event'

RSpec.describe Triage::CustomerContributionMergedNotifier do
  subject { described_class.new(event, messenger: messenger_stub) }

  let(:event_author) { Triage::Event::ENG_PROD_TEAM_USERNAMES.first }
  let(:type_label) { 'bug' }
  let(:url) { 'http://gitlab.com/mr_url' }
  let(:org_name) { 'org' }
  let(:event) do
    instance_double('Triage::MergeRequestEvent',
      from_gitlab_org?: true,
      wider_community_author?: true,
      merge_request?: true,
      merge_event?: true,
      author_id: 42,
      label_names: [*type_label],
      url: url)
  end

  let(:messenger_stub) { double }

  before do
    allow(Triage::OrgByUsernameLocator).to receive(:locate_org).and_return(org_name)
    allow(messenger_stub).to receive(:ping)
  end

  it_behaves_like 'customer contribution processor slack options', '#mrarr-wins'

  describe '#process' do
    shared_examples 'message posting' do
      it 'posts a customer contribution message' do
        body = <<~MARKDOWN
          > Customer MR Merged -
          > Organization: #{described_class::CUSTOMER_PORTAL_URL}#{org_name}
          > Contribution Type: #{contribution_type}
          > MR Link: #{url}
        MARKDOWN

        subject.process
        expect(messenger_stub).to have_received(:ping).exactly(1).times.with(body)
      end
    end

    it_behaves_like 'customer contribution processor #process'

    context 'when event is not for a merged mr' do
      before do
        allow(event).to receive(:merge_event?).and_return(false)
      end

      it_behaves_like 'no message posting'
    end
  end
end
