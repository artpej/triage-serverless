# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/rack/webhook_event'

describe Triage::Rack::WebhookEvent do
  let(:fake_app) { double }
  let(:app)      { described_class.new(fake_app) }
  let(:env)      { { Rack::RACK_INPUT => StringIO.new("foo") } }
  let(:response) { app.call(env) }
  let(:status)   { response[0] }
  let(:body)     { response[2][0] }

  context 'when JSON is invalid' do
    it 'returns a 400 error' do
      expect(status).to eq(400)
      expect(body).to eq(JSON.dump(status: :error, error: "JSON::ParserError", message: "434: unexpected token at 'foo]'"))
    end
  end

  context 'when JSON is valid' do
    let(:event_hash) { { "foo" => "bar" } }
    let(:event) { double('Triage::Event') }
    let(:env) { { Rack::RACK_INPUT => StringIO.new(JSON.dump(event_hash)) } }

    it 'calls the rest of the middleware stack and pass the event in the env' do
      expect(Triage::Event).to receive(:build).with(event_hash).and_return(event)
      expect(fake_app).to receive(:call).with(env.merge(event: event))

      response
    end
  end
end
